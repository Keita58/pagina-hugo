+++
title = 'Documentació Hugo'
date = 2024-05-15T20:10:47+02:00
draft = false
+++

# Passos a seguir

He agafat com a tutorial per a crear la pàgina la pàgina web oficial d'[Hugo](https://gohugo.io/getting-started/quick-start/), on hi ha un tutorial per a fer-te la teva pàgina web.

## Preliminars

He hagut d'instal·lar l'aplicatiu [Hugo](https://gohugo.io/installation/) i tenir ja instal·lat git (per a utilitzar la seva terminal, ja que tant elCMP com el PowerShell de Windows no van bé).

## Crear la pàgina en sí

Ara que ja tinc els aplicatius necessaris, puc obrir la terminal de git i escriure "hugo sew site <<nom de la carpeta>>". Ara dins de la carpeta que he especificat tinc tots els documents necessaris per a crear la pàgina web.
He anat a dins d'aquesta amb la terminal, he iniciat un nou repositori git (important si vull posar la pàgina al Gitlab), he creat els documents de .gitignore i .gitmodules i en aquest segon he afegit un submòdul amb la comanda "git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke", amb el que m'ha posat el tema d'ananke a la pàgina web.
En el hugo.toml que s'ha creat al fer el hugo site new he hagut d'afegit una línia al final "theme = 'ananke'" per a que reconegués el submòdul que he afegit i amb això ja està la pàgina creada. Ara només falta afegir diversos posts per comprovar que estigui tot bé.

## Crear posts a la pàgina web

Per a crear posts a la pàgina ho he fet amb la comanda "hugo new content posts/<<nom del post>>.md" i llestos, ara ja tinc el primer post on afegir la informació que vulgui, com per exemple la documentació a seguir per a crear una nova pàgina d'Hugo 😉